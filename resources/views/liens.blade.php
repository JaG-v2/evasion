@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h2 class="orange">Liens utiles </h2>
			<div class="row">
				<div class="col-md-4 center"><a href="http://www.nicetourisme.com" target="_blank"><img src="{{url('/img/links/nicetourisme.png')}}"class="center img-responsive" alt="Nice tourisme"></a></div>
				<div class="col-md-4 center"><a href="http://www.nice.aeroport.fr" target="_blank"><img src="{{url('/img/links/niceaeroport.png')}}"class="center img-responsive" alt="Aeroport de Nice"></a></div>
				<div class="col-md-4 center"><a href="http://www.ville-nice.fr" target="_blank"><img src="{{url('/img/links/villedenice.png')}}"class="center img-responsive" alt="Ville de Nice"></a></div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-4 center"><a href="http://www.velobleu.org" target="_blank"><img src="{{url('/img/links/velobleu.png')}}"class="center img-responsive" alt="Vélo Bleu"></a></div>
				<div class="col-md-4 center"><a href="http://www.ter-sncf.com/Regions/Paca" target="_blank"><img src="{{url('/img/links/sncf.png')}}"class="center img-responsive" alt="SNCF"></a></div>
				<div class="col-md-4 center"><a href="http://www.nicecotedazur.org" target="_blank"><img src="{{url('/img/links/metropole.png')}}"class="center img-responsive" alt="Metropole Nice côte d'azur"></a></div>
			</div>
			<br><br>
		</div>
	</div>
</div>
@endsection
