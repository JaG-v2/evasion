@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<blockquote class="blue">« Vous êtes toujours au centre de notre attention, au-delà de votre satisfaction, notre souhait et d’anticiper vos attentes pour vous ouvrir de nouveaux horizons et donner des ailes à vos envies  »</blockquote>
			<h4>L’équipe <span class="blue">NICE</span> <span class="orange">EVASION</span> met en œuvre tous les moyens nécessaires pour optimiser la qualité de ses services. Ainsi, des engagements qualité concernant le personnel, la réalisation des services, et le matériel, sont mis en place afin de répondre à vos exigences. </h4>
		</div>		
	</div>
	<div class="row margin-t-10">
		<div class="col-md-6 margin-t-10">
			<ul class="check">
				<li><strong class="blue">Qualité du personnel : </strong><br>
					Une équipe commerciale professionnelle, des conducteurs sélectionnés et formés par nos soins sur leur rôle commercial, la sécurité ainsi qu’une conduite rationnelle.
				</li>
				<li><strong class="blue">Qualité des véhicules  : </strong><br>
					Outre le confort, la qualité de nos véhicules  de pointe vous assure un voyage agréable
				</li>
				<li><strong class="blue">Pour tout  transport en autocar : </strong><br>
					Nous avons souscrit une assurance Responsabilité civile professionnelle pour garantir tous dommages.

				</li>
				<li><strong class="blue">Développement durable  : </strong><br>
					Plan spécifique de développement durable, Stages de conduite dite « rationnelle, Investissement dans des véhicules « propres », diminution des rejets et risques de pollution liés aux activités de maintenance et réparation. 
				</li>
				<li><strong class="blue">Réglementation sur les transports de voyageurs  : </strong><br>
					Notre qualité de service, comprend aussi notre engagement du respect de la réglementation. Afin de garantir un maximum de sécurité à nos clients et à nos conducteurs, nous respectons les règles élémentaires auxquelles toute entreprise de transport de voyageurs est soumise.
				</li>
			</ul>
		</div>
		<div class="col-md-6">
			<img src="{{url('/img/quality.jpg')}}" alt="Engagements qualité" class="img-responsive">
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-md-2">
			<img src="{{url('/img/cg.gif')}}" alt="CG06" class="img-responsive">
		</div>
		<div class="col-md-2">
			<img src="{{url('/img/nice.jpg')}}" alt="Nice" class="img-responsive">
		</div>
		<div class="col-md-2">
			<img src="{{url('/img/paca.jpg')}}" alt="PACA" class="img-responsive">
		</div>
		<div class="col-md-2">
			<img src="{{url('/img/men.gif')}}" alt="MEN" class="img-responsive">
		</div>
		<div class="col-md-2">
			<img src="{{url('/img/unice.png')}}" alt="Unice" class="img-responsive">
		</div>
		<div class="col-md-2">
			<img src="{{url('/img/apf.jpg')}}" alt="APF" class="img-responsive">
		</div>
	</div>
</div>
@endsection
