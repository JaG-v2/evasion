@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div id="carousel" class="carousel slide center" data-ride="carousel">
		  <!-- Indicators -->
		  <ol class="carousel-indicators">
		    <li data-target="#carousel" data-slide-to="0" class="active"></li>
		    <li data-target="#carousel" data-slide-to="1"></li>
		    <li data-target="#carousel" data-slide-to="2"></li>
		    <li data-target="#carousel" data-slide-to="3"></li>
		    <li data-target="#carousel" data-slide-to="4"></li>
		    <li data-target="#carousel" data-slide-to="5"></li>
		    <li data-target="#carousel" data-slide-to="6"></li>
		  </ol>

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner" role="listbox">
		    <div class="item active">
		      <img src="{{url('/img/slide/1.jpg')}}" alt="Photos Nice Evasion">
		    </div>
		    <div class="item">
		      <img src="{{url('/img/slide/2.jpg')}}" alt="Photos Nice Evasion">
		    </div>
		    <div class="item">
		      <img src="{{url('/img/slide/3.jpg')}}" alt="Photos Nice Evasion">
		    </div>
		    <div class="item">
		      <img src="{{url('/img/slide/4.jpg')}}" alt="Photos Nice Evasion">
		    </div>
		    <div class="item">
		      <img src="{{url('/img/slide/5.jpg')}}" alt="Photos Nice Evasion">
		    </div>
		    <div class="item">
		      <img src="{{url('/img/slide/6.jpg')}}" alt="Photos Nice Evasion">
		    </div>
		  </div>

		  <!-- Controls -->
		  <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
		    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		    <span class="sr-only">Précédent</span>
		  </a>
		  <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
		    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		    <span class="sr-only">Suivant</span>
		  </a>
		</div>
	</div>
</div>
@endsection
