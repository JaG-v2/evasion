@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1 class="center">Nous contacter</h1>
		</div>		
		<div class="col-md-6 margin-t-10">
			<div class="row">
				<div class="col-md-2">
					<img src="{{url('/img/phone.jpg')}}" alt="" class="img-responsive">
				</div>
				<div class="col-md-10">
					<h3 class="blue">
						06 20 78 49 46 <br>
						06 18 20 48 07
					</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-md-2">
					<img src="{{url('/img/email.jpg')}}" alt="" class="img-responsive">
				</div>
				<div class="col-md-10">
					<h3 class="blue">
						<a href="mailto:bus@nice-evasion.com">bus@nice-evasion.com</a>
					</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-md-2">
					<img src="{{url('/img/address.jpg')}}" alt="" class="img-responsive">
				</div>
				<div class="col-md-10">
					<h3 class="blue">
						52 boulevard René Cassin 06200 Nice 
					</h3>
				</div>
			</div>
		</div>
		<div class="col-md-6 margin-t-10">	
			{!! Form::open(['url' => 'contact', "class" => "form-horizontal"]) !!}		
			<form class="form-horizontal">
				<h3 class="orange center">Formulaire de contact</h2>
			  	<div class="form-group margin-t-20">
				    <label for="" class="col-sm-4 control-label">Nom Prénom *</label>
				    <div class="col-sm-8">
				    	{!! Form::text('name', null, ["id" => "name", "placeholder" => "Nom Prénom", "class" => "form-control"]) !!}
				    </div>
			  	</div>
			  	<div class="form-group">
				    <label for="" class="col-sm-4 control-label">Email *</label>
				    <div class="col-sm-8">
				    	{!! Form::text('email', null, ["id" => "email", "placeholder" => "Email", "class" => "form-control"]) !!}
				    </div>
			  	</div>
			  	<div class="form-group">
				    <label for="" class="col-sm-4 control-label">Téléphone *</label>
				    <div class="col-sm-8">
				    	{!! Form::text('phone', null, ["id" => "phone", "placeholder" => "Téléphone", "class" => "form-control"]) !!}
				    </div>
			  	</div>
			  	<div class="form-group">
				    <label for="" class="col-sm-4 control-label">Message *</label>
				    <div class="col-sm-8">				    	
				    	{!! Form::textarea('content', null, ["id" => "content", "placeholder" => "Message", "class" => "form-control",  "rows" => "3"]) !!}
				    </div>
			  	</div>
			  	<div class="form-group">
				    <div class="col-sm-12 center">
				      <button type="submit" class="btn btn-primary btn-lg">Envoyer</button>
				    </div>
			  	</div>
			</form>
		</div>
	</div>
</div>
@endsection
