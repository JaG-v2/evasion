@extends('home')

@section('form')
	{!! Form::open(['url' => '/', "class" => "form-horizontal"]) !!}
		<h2 class="orange center">Devis Express</h2>
		<div class="form-group margin-t-20">
		    <label for="" class="col-sm-4 control-label">Ville de départ *</label>
		    <div class="col-sm-8">
		    	{!! Form::text('start_city', null, ["id" => "start_city", "placeholder" => "Ville de départ", "class" => "form-control"]) !!}
		    </div>
	  	</div>
		<div class="form-group margin-t-20">
		    <label for="" class="col-sm-4 control-label">Ville d'arrivée *</label>
		    <div class="col-sm-8">
		    	{!! Form::text('end_city', null, ["id" => "end_city", "placeholder" => "Ville d'arrivée", "class" => "form-control"]) !!}
		    </div>
	  	</div>
		<div class="form-group margin-t-20">
		    <label for="" class="col-sm-4 control-label">Nombre de jours *</label>
		    <div class="col-sm-8">
		    	{!! Form::text('days', null, ["id" => "days", "placeholder" => "Nombre de jours", "class" => "form-control"]) !!}
		    </div>
	  	</div>
		<div class="form-group margin-t-20">
		    <label for="" class="col-sm-4 control-label">Date de départ *</label>
		    <div class="col-sm-8">
		    	{!! Form::text('start_date', null, ["id" => "start_date", "placeholder" => "Date de départ", "class" => "form-control"]) !!}
		    </div>
	  	</div>
		<div class="form-group margin-t-20">
		    <label for="" class="col-sm-4 control-label">Date d'arrivée *</label>
		    <div class="col-sm-8">
		    	{!! Form::text('end_date', null, ["id" => "end_date", "placeholder" => "Date d'arrivée", "class" => "form-control"]) !!}
		    </div>
	  	</div>
		<div class="form-group margin-t-20">
		    <label for="" class="col-sm-4 control-label">Nombre de passagers *</label>
		    <div class="col-sm-8">
		    	{!! Form::text('passengers', null, ["id" => "passengers", "placeholder" => "Nombre de passagers", "class" => "form-control"]) !!}
		    </div>
	  	</div>
	  	<div class="form-group">
		    <div class="col-sm-12 center">
		      <button type="submit" class="btn btn-primary btn-lg">Mon devis</button>
	    	</div>
	  	</div>
	</form>
@endsection
