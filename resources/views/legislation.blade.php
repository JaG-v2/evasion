@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h2 class="orange">Législations </h2>
			Les autocars Bus <span class="blue">NICE</span> <span class="orange">EVASION</span> s’engage pour que l’autocar reste un mode de transport plus sûr, plus économique, plus écologique et plus accueillant.<br><br>

			<h2 class="blue">Notre priorité, l’application et le respect de la législation. </h2><br>

			Afin que le transport en autocar reste l’un des modes de déplacement terrestre les plus sûrs, les autocars Bus <span class="blue">NICE</span> <span class="orange">EVASION</span> veille en permanence au respect de la législation, aux équipements obligatoire et à  la formation des conducteurs.<br><br>


		</div>
	</div>
</div>
@endsection
