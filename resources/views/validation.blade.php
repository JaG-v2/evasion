@extends('home')

@section('form')
	{!! Form::open(['url' => 'validation', "class" => "form-horizontal"]) !!}
		<h2 class="orange center">Devis Express - Etape 2/2</h2>
			<div class="form-group margin-t-20">
			    <label for="" class="col-sm-4 control-label">Nom Prénom *</label>
			    <div class="col-sm-8">
			    	{!! Form::text('name', null, ["id" => "name", "placeholder" => "Nom Prénom", "class" => "form-control"]) !!}
			    </div>
		  	</div>
			<div class="form-group margin-t-20">
			    <label for="" class="col-sm-4 control-label">Société *</label>
			    <div class="col-sm-8">
			    	{!! Form::text('company', null, ["id" => "company", "placeholder" => "Société", "class" => "form-control"]) !!}
			    </div>
		  	</div>
		  	<div class="form-group">
			    <label for="" class="col-sm-4 control-label">Email *</label>
			    <div class="col-sm-8">
			    	{!! Form::text('email', null, ["id" => "email", "placeholder" => "Email", "class" => "form-control"]) !!}
			    </div>
		  	</div>
		  	<div class="form-group">
			    <label for="" class="col-sm-4 control-label">Téléphone *</label>
			    <div class="col-sm-8">
			    	{!! Form::text('phone', null, ["id" => "phone", "placeholder" => "Téléphone", "class" => "form-control"]) !!}
			    </div>
		  	</div>
		  	<div class="form-group">
			    <label for="" class="col-sm-4 control-label">Message</label>
			    <div class="col-sm-8">				    	
			    	{!! Form::textarea('content', null, ["id" => "content", "placeholder" => "Message", "class" => "form-control",  "rows" => "3"]) !!}
			    </div>
		  	</div>
		  	<div class="form-group">
			    <div class="col-sm-12 center">
			      <button type="submit" class="btn btn-primary btn-lg">Envoyer</button>
			    </div>
		  	</div>
	</form>
@endsection
