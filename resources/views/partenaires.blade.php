@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h2 class="orange">Devenez  partenaires </h2>
			Vous  proposez un service prestigieux qui correspond également à nos préoccupations. Nous sommes une  équipe dynamique et vous pouvez compter sur nos services de qualité pour développer continuellement de nouvelles perspectives dans le domaine du transport.<br><br>

			Devenez notre partenaire afin de proposer le meilleur produit à nos clients respectifs, dans une relation de confiance,  en remplissant simplement <a href="{{url('/contact')}}">ce formulaire</a><br><br>


		</div>
	</div>
</div>
@endsection
