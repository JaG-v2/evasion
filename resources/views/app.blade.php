<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>BUS NICE EVASION</title>

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">

	<link href="{{ asset('/css/custom.css') }}" rel="stylesheet">

	<!-- Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container">
			<div class="pull-right orange slogan">Location d'autocars avec chauffeur<br>au meilleur tarif</div>
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a href="{{ url('/') }}"><img src="{{url('/img/logo.jpg')}}" alt="BUS NICE EVASION" class="img-responsive"></a>
				<a class="navbar-brand" href="{{ url('/') }}"><span class="blue">BUSNICE</span> <span class="orange">EVASION</span></a>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li @if(Request::url() == url('/')) class="active" @endif><a href="{{ url('/') }}">Accueil</a></li>
						<li @if(Request::url() == url('/vehicules')) class="active" @endif><a href="{{ url('/vehicules') }}">Véhicules</a></li>
						<li @if(Request::url() == url('/destinations')) class="active" @endif><a href="{{ url('/destinations') }}">Destinations</a></li>
						<li @if(Request::url() == url('/engagements-qualites')) class="active" @endif><a href="{{ url('/engagements-qualites') }}">Engagements qualité</a></li>
						<li @if(Request::url() == url('/photos')) class="active" @endif><a href="{{ url('/photos') }}">Photos</a></li>
						<li @if(Request::url() == url('/contact')) class="active" @endif><a href="{{ url('/contact') }}">Contact</a></li>
					</ul>
				</div>
			</div>
		</div>
	</nav>
	<div class="container">
		@if(Session::has('success'))
			<div class="alert alert-success" role="alert">
				{{Session::get('success')}}
			</div>
		@endif
		@if(Session::has('danger'))
			<div class="alert alert-danger" role="alert">
				{{Session::get('danger')}}
			</div>
		@endif
	</div>
	@yield('content')
	<footer class="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<h4 class="orange">Départs :</h4>
					<a href="{{url('/location/bus/nice')}}">Location Bus Nice Evasion</a><br>
					<a href="{{url('/location/bus/monaco')}}">Location Bus Monaco Evasion</a><br>
					<a href="{{url('/location/bus/cannes')}}">Location Bus Cannes Evasion</a><br>
					<a href="{{url('/location/bus/sophia-antipolis')}}">Location Bus Sophia Antipolis Evasion</a><br>
					
				</div>
				<div class="col-md-4">
					<h4 class="orange">Voyages en autocars  :</h4>
					<a href="{{url('/tourisme-evenementiel')}}">Location autocars de  tourisme</a><br>
					<a href="{{url('/transport-scolaire-periscolaire')}}">Location autocars scolaires, périscolaires </a><br>
					<a href="{{url('/transferts')}}">Location autocars transferts</a><br>
					<!--a href="{{url('/associations')}}">Location autocars associations, sportifs</a><br--></div>
				<div class="col-md-4">
					<h4 class="orange">La Location aux meilleurs tarifs :</h4>
					<a href="{{url('/partenaires')}}"> Devenez Partenaires</a><br>
					<a href="{{url('/legislation')}}"> Législations</a><br>
					<a href="{{url('/liens')}}">Liens utiles</a><br>
					<a href="{{url('/contact')}}">Nous contacter</a><br></div>
			</div>
			<div class="row">
				<div class="col-md-12 center">&copy; {{date('Y')}} Nice Evasion</div>
			</div>
		</div>
	</footer>

	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
</body>
</html>
