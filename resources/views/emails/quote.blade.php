Bonjour,<br>
Un nouveau devis du site internet :<br>
Ville de départ : {{$start_city}}<br>
Ville d'arrivée : {{$end_city}}<br>
Nombre de jours : {{$days}}<br>
Date de départ : {{$start_date}}<br>
Date de retour : {{$end_date}}<br>
Passagers : {{$passengers}}<br>
Nom prénom : {{$name}}<br>
Email : {{$email}}<br>
Téléphone : {{$phone}}<br>
Message : {{$content}}
