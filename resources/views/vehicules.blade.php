@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<p class="description">
				De la voiture avec chauffeur à l’autocar grand tourisme, nos véhicules sont tous habilités «  TOURISME  » par un organisme agrée vous garantissant un service personnalisé de qualité.
			</p>
		</div>		
	</div>
	<div class="row">
		<div class="col-md-12">
			Tous nos véhicules sont équipés de :
		</div>
	</div>
	<div class="row margin-t-10">
		<div class="col-md-3 margin-t-10">
			<ul class="check">
				<li>ABS</li>
				<li>Air Conditionné</li>
				<li>Sièges Inclinables</li>
				<li>Micro</li>
				<li>Hi-fi</li>
				<li>Réfrigérateur</li>
			</ul>
		</div>
		<div class="col-md-3 margin-t-10">
			<ul class="check">
				<li>ASR</li>
				<li>Ethylotest Anti-Démarrage</li>
				<li>Vidéo</li>
				<li>DVD</li>
				<li>WC</li>
				<li>Cafetière</li>
			</ul>
		</div>
		<div class="col-md-6">
			<img src="{{url('/img/interior.jpg')}}" alt="Véhicules" class="img-responsive">
		</div>
	</div>
</div>
@endsection
