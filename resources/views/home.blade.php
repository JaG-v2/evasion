@extends('app')

@section('content')
<div id="content-wrap" class="container">
	<div class="widget-container">
		<div class="container"> 
			<div class="col-md-6 col-md-offset-6">
				<div class="panel panel-default">
					<div class="panel-body">
						@yield('form')
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-body">
						<h2 class="orange center">Contact 24/24</h2>
						<h3 class="blue center margin-t-20">
							06 20 78 49 46<br>
							06 18 20 48 07
						</h3>
						  
					</div>
				</div>
			</div>
		</div>
	</div>
	<figure id="main-img">
		<img src="{{url('/img/bus.jpg')}}" alt="Bus Nice Evasion">
	</figure>
</div>
@endsection
