@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h2 class="orange">Transports scolaires et périscolaire: </h2>
			Conscient que tous les établissements scolaires ne disposent d’installations sportives, les autocars Bus <span class="blue">NICE</span> <span class="orange">EVASION</span>  se ferons  le plaisir de supervisée à vos côtés  toutes vos sortis<br><br>
			Selon vos besoins : Nombre d’élèves, destinations, horaires…,  l’équipe Bus <span class="blue">NICE</span> <span class="orange">EVASION</span>  étudiera votre demande avec soin et vous proposerons la gamme de véhicules la mieux adapté à votre convenance et  budget.<br><br>
			Tous nos autocars sont équipés de ceinture de sécurité et de système d’éthylotest Anti-démarrage, obligatoire pour le transport d’enfants. <br><br>

			N’hésite pas à nous fait part de votre demande de devis gratuit ou de votre appel d’offre vous pourrais apprécier la différence.<br><br>


		</div>
	</div>
</div>
@endsection
