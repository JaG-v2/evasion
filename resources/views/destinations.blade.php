@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h2 class="orange">LA MOBILITÉ SUR-MESURE</h2>
			Spécialisé sur l'activité location d’autocars avec chauffeurs pour tous types de déplacements :
			<ul class="check">
				<li>Tourismes</li>
				<li>Evènementiels</li>
				<li>Réceptifs</li>
				<li>Scolaires, périscolaires</li>
			</ul>
		</div>		
	</div>
	<div class="row">
		<div class="col-md-12">
			Après une étude précise de vos besoins, (effectifs, budgets, horaire....), notre équipe, à votre entière dispositions, vous proposera la gamme de véhicules la mieux adapté, afin de vous proposer un  transport sur-mesure. <br><hr><br>
			Les Autocars Nice Evasion  ont souhaité garder un fonctionnement et un effectif « familial », pour pouvoir répondre au mieux aux demandes de nos clients, établir un rapport de confiance. Ainsi sont à votre disposition :

		</div>
	</div>
	<div class="row margin-t-10">
		<div class="col-md-6 margin-t-10">
			<ul class="check">
				<li>Une équipe dynamique</li>
				<li>Un interlocuteur unique à votre écoute joignable 24/24</li>
				<li>Une grande faculté d’adaptation </li>
				<li>Une large gamme de véhicules des plus performants </li>
				<li>Nos conducteurs spécifiquement formés</li>
			</ul>
		</div>
		<div class="col-md-6">
			<img src="{{url('/img/destination.jpg')}}" alt="Destination" class="img-responsive">
		</div>
	</div>
	<div class="row">
		<h2 class="blue">Choisissez votre destination, Nice <span class="orange">Evasion</span> s’occupe du reste…</h2>
	</div>
</div>
@endsection
