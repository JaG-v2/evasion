@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<h1 class="orange">Location Autocar <span class="blue">BUS</span> {{ucwords(str_replace('-', ' ', $city))}} <span class="orange">EVASION</span></h1>
		« En provenance de {{ucwords(str_replace('-', ' ', $city))}} ou à destination de {{ucwords(str_replace('-', ' ', $city))}}, <span class="blue">BUS</span> {{ucwords(str_replace('-', ' ', $city))}} <span class="orange">EVASION</span> vous accompagne en devenant votre partenaire privilégié de transport en autocars.<br><br>
		Vous pouvez faire votre demande de devis directement sur notre site, pour recevoir une proposition sur la location d'un autobus à destination de {{ucwords(str_replace('-', ' ', $city))}} ou au départ de {{ucwords(str_replace('-', ' ', $city))}}.<br><br>
		N'hésitez pas à nous contacter pour tout renseignement.<br><br>
		Nous souhaitons une bonne navigation sur notre site de location d’autocars <span class="blue">BUS</span> {{ucwords(str_replace('-', ' ', $city))}} <span class="orange">EVASION</span>. »

	</div>
</div>
@endsection
