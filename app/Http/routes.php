<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

Route::post('/', 'HomeController@handleIndex');

Route::get('/validation', 'HomeController@validation');

Route::post('/validation', 'HomeController@handleValidation');

Route::get('/vehicules', 'HomeController@vehicules');

Route::get('/destinations', 'HomeController@destinations');

Route::get('/engagements-qualites', 'HomeController@quality');

Route::get('/photos', 'HomeController@photos');

Route::get('/contact', 'HomeController@contact');

Route::post('/contact', 'HomeController@handleContact');

Route::get('/location/bus/{city}', 'HomeController@location');

Route::get('/tourisme-evenementiel', 'HomeController@tourisme');

Route::get('/transport-scolaire-periscolaire', 'HomeController@scolaire');

Route::get('/transferts', 'HomeController@transferts');

Route::get('/associations', 'HomeController@associations');

Route::get('/partenaires', 'HomeController@partenaires');

Route::get('/legislation', 'HomeController@legislation');

Route::get('/liens', 'HomeController@liens');

