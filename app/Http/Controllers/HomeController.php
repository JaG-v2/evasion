<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('index');
	}
	
	public function validation()
	{
		if (!Session::has('data'))
			return redirect('/');
		return view('validation');
	}

	public function handleIndex()
	{
		$rules = [
			'start_city' => 'required',
			'end_city'   => 'required',
			'days'       => 'required',
			'start_date' => 'required',
			'end_date'   => 'required',
			'passengers' => 'required',
	    ];
		$validator = Validator::make(Input::all(), $rules);
	    if ($validator->fails())
	    {
	    	Session::flash('danger', 'Veuillez compléter les champs marqués d\'une étoile');
	    	return redirect()->back()->withErrors($validator)->withInput();
	    }
	    $data = [
			'start_city' => e(Input::get('start_city')),
			'end_city'   => e(Input::get('end_city')),
			'days'       => e(Input::get('days')),
			'start_date' => e(Input::get('start_date')),
			'end_date'   => e(Input::get('end_date')),
			'passengers' => e(Input::get('passengers')),
	    ];
	    Session::put('data', $data);
    	return redirect('/validation');
	}
	
	public function handleValidation()
	{
		if (!Session::has('data'))
			return redirect('/');
		$rules = [
			'name'    => 'required',
			'email'   => 'required',
			'phone'   => 'required'
	    ];
		$validator = Validator::make(Input::all(), $rules);
	    if ($validator->fails())
	    {
	    	Session::flash('danger', 'Veuillez compléter les champs marqués d\'une étoile');
	    	return redirect()->back()->withErrors($validator)->withInput();
	    }
	    $data = array_merge(Session::get('data'), Input::all());
	    Mail::send('emails.quote', $data, function($message)
		{
		    $message->to('bus@nice-evasion.com', 'Nice Evasion')->subject('Demande de devis du site internet');
		});
    	Session::flash('success', 'Merci pour votre message, nous vous répondrons dans les meilleurs délais');
    	return redirect('/');
	}

	public function vehicules()
	{
		return view('vehicules');
	}

	public function destinations()
	{
		return view('destinations');
	}

	public function quality()
	{
		return view('quality');
	}

	public function photos()
	{
		return view('photos');
	}

	public function contact()
	{
		return view('contact');
	}

	public function handleContact()
	{
		$rules = [
			'name'    => 'required',
			'email'   => 'required',
			'phone'   => 'required',
			'content' => 'required',
	    ];
		$validator = Validator::make(Input::all(), $rules);
	    if ($validator->fails())
	    {
	    	Session::flash('danger', 'Veuillez compléter les champs marqués d\'une étoile');
	    	return redirect()->back()->withErrors($validator)->withInput();
	    }
	    Mail::send('emails.message', Input::all(), function($message)
		{
		    $message->to('bus@nice-evasion.com', 'Nice Evasion')->subject('Message du site internet');
		});

    	Session::flash('success', 'Merci pour votre message, nous vous répondrons dans les meilleurs délais');
    	return redirect('/');
	}

	public function location($city)	
	{
		return view('location', ['city' => $city]);
	}

	public function tourisme()
	{
		return view('tourisme');
	}

	public function scolaire()
	{
		return view('scolaire');
	}

	public function transferts()
	{
		return view('transferts');
	}

	public function associations()
	{
		return view('associations');
	}

	public function partenaires()
	{
		return view('partenaires');
	}

	public function legislation()
	{
		return view('legislation');
	}

	public function liens()
	{
		return view('liens');
	}

}
